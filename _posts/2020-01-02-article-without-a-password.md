---
layout: post
title: This article uses the global password
description: A demonstration of encrypted content
---

Of course, the source code of the site shouldn't be published.  We may
provide ways to encrypt the actual contents of the site on the source
code --someday!
