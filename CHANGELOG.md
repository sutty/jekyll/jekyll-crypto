# Changelog

## 0.1.3 -- 2020-11-04

* Hide ciphertext

## 0.1.2 - 2020-09-28

* Support Ruby 2.7

## 0.1.1 - 2020-03-06

* Prevent password autocompletion

* Make encryption optional specifying `password: false` in front matter.

## 0.1.0 - 2020-03-04

First release!  Posts contents are encrypted and authenticated with
AES-GCM-256 and passwords are hashed with PBKDF2-SHA256.
